import React from 'react';
import ReactDOM from 'react-dom';
import { BrowserRouter as Router } from 'react-router-dom';
import { Provider } from 'react-redux';
import App from './components/App.container';
import configureStore from './configs/configureStore';
import rootSaga from './configs/rootSaga';

import './css/index.css';
import './css/style.css';

const store = configureStore();
store.runSaga(rootSaga);

const root = document.getElementById('root');

const render = () => {
  ReactDOM.render(
    <Provider store={store}>
      <Router>
        <App />
      </Router>
    </Provider>,
    root
  );
};

render();
