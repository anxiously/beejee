import React, { Fragment, Component } from 'react';
import { Route, Switch, Redirect } from 'react-router-dom';
import { routes } from '../configs/routes';

import AuthWrap from './PrivateRoutes/AuthWrap.container';
import Tasks from './tasks/task.container';
import Login from './login/login.container';
import NewTask from './tasks/newTask/newTask.container';

import '../css/App.css';

class App extends Component {
  state = {};

  render() {
    // const { auth } = this.props;
    return (
      <Fragment>
        <div className="admin-wrapper">
          <div className="dash-main dash-main--admin">
            <Switch>
              {/* <Route exact path={routes.login} component={() => (auth ? <Redirect to={routes.root} /> : <Login />)} /> */}
              <AuthWrap exact path={routes.task} component={Tasks} />
              <AuthWrap exact path={routes.newTask} component={NewTask} />
              <Redirect to={routes.root} />
            </Switch>
          </div>
        </div>
      </Fragment>
    );
  }
}

export default App;
