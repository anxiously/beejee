import { connect } from 'react-redux';

import Tasks from './task.component';
import { getTasks } from './task.action';

const mapStateToProps = state => ({
  tasks: state.getTasks.tasksList.Tasks,
  totalPage: state.getTasks.tasksList.TotalPages,
});

export default connect(
  mapStateToProps,
  { getTasks }
)(Tasks);
