import React, { Fragment, Component } from 'react';
import TaskListItem from './taskListItem';

class TaskList extends Component {
  state = {
    sortField: 'id',
    sortDirection: 'asc',
  };

  sortTasks = e => {
    const { sortField, sortDirection } = this.state;
    const { getTasks } = this.props;
    this.setState(
      {
        sortField: e.target.getAttribute('name'),
        sortDirection: sortDirection === 'asc' ? 'desc' : 'asc',
      },
      () => {
        getTasks(this.state);
      }
    );
  };

  // setTimeout(() => {
  //   getTasks({ sort_field: sortField, sort_direction: sortDirection });
  // }, 10000);

  render() {
    const { tasks } = this.props;
    return (
      <table className="table">
        <thead className="thead-dark">
          <tr>
            <th name="id" onClick={this.sortTasks} scope="col">
              Id
            </th>
            <th name="username" onClick={this.sortTasks} scope="col">
              Username
            </th>
            <th name="email" onClick={this.sortTasks} scope="col">
              Email
            </th>
            <th scope="col">Text</th>
          </tr>
        </thead>
        <tbody>
          {tasks.map((task, index) => (
            <TaskListItem task={task} key={+index} />
          ))}
        </tbody>
      </table>
    );
  }
}

export default TaskList;
