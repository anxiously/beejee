import React from 'react';

const TasksLIstItem = ({ task }) => {
  return (
    <tr>
      <th scope="row">{task.id}</th>
      <td>{task.username}</td>
      <td>{task.email}</td>
      <td>{task.text}</td>
    </tr>
  );
};

export default TasksLIstItem;
