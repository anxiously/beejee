import React, { Component, Fragment } from 'react';

class NewTask extends Component {
  state = {
    email: '',
    user: '',
    text: '',
  };

  componentDidUpdate(prevProps) {
    const { status } = this.props;
    if (prevProps.status !== status && status === 'success') {
      this.setState({
        email: '',
        user: '',
        text: '',
      });
    }
  }

  fullMessage = ({ target: { name, value } }) => {
    this.setState({
      [name]: value,
    });
  };

  addTask = () => {
    const { createTask } = this.props;
    const { email, user, text } = this.state;
    const data = new FormData();
    data.append('username', user);
    data.append('email', email);
    data.append('text', text);
    createTask(data);
  };

  render() {
    const { email, user, text } = this.state;

    return (
      <Fragment>
        <form>
          <div className="form-group">
            <label htmlFor="exampleFormControlInput1">Email address</label>
            <input
              onChange={this.fullMessage}
              value={email}
              name="email"
              type="email"
              className="form-control"
              id="exampleFormControlInput1"
              placeholder="name@example.com"
            />
          </div>
          <div className="form-group">
            <label htmlFor="exampleFormControlInput1">User name</label>
            <input
              onChange={this.fullMessage}
              value={user}
              name="user"
              type="text"
              aria-label="First name"
              className="form-control"
            />
          </div>
          <div className="form-group">
            <label htmlFor="exampleFormControlTextarea1">Example textarea</label>
            <textarea
              value={text}
              onChange={this.fullMessage}
              name="text"
              className="form-control"
              id="exampleFormControlTextarea1"
              rows="3"
            />
          </div>
          <button onClick={this.addTask} type="button" className="btn btn-primary btn-lg btn-block">
            CREATE TASK
          </button>
        </form>
      </Fragment>
    );
  }
}

export default NewTask;
