import { takeLatest, all } from 'redux-saga/effects';

import { createTaskWorker, createTask } from './newTask.action';

export function* newTasksWatcher() {
  yield all([takeLatest(createTask.TRIGGER, createTaskWorker)]);
}
