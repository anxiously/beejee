import { handleActions } from 'redux-actions';
import { combineReducers } from 'redux';

import * as actions from './newTask.action';

const statusCreate = handleActions(
  {
    [actions.createTask.REQUEST]() {
      return 'request';
    },
    [actions.createTask.SUCCESS]() {
      return 'success';
    },
    [actions.createTask.FAILURE]() {
      return 'failure';
    },
  },
  'none'
);

const createTask = combineReducers({
  statusCreate,
});

export default createTask;
