import { connect } from 'react-redux';

import NewTask from './newTask.component';
import { createTask } from './newTask.action';

const mapStateToProps = state => ({
  status: state.createTask.statusCreate,
});

export default connect(
  mapStateToProps,
  { createTask }
)(NewTask);
