import { createRoutine } from 'redux-saga-routines';
import { put, call } from 'redux-saga/effects';

import api from '../../../configs/api';

export const createTask = createRoutine('CREATE_TASKS');

export function* createTaskWorker({ payload }) {
  yield put(createTask.request());
  try {
    const res = yield call(() => api.createTask(payload));
    console.log(res);
    if (res.data.status === 'ok') {
      yield put(createTask.success(res.data.message));
    } else {
      yield put(createTask.failure());
    }
  } catch (err) {
    console.log('catch', err);
    yield put(createTask.failure());
  }
}
