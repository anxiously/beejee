import { handleActions } from 'redux-actions';
import { combineReducers } from 'redux';

import * as action from './newTask/newTask.action';
import * as actions from './task.action';

const initialState = {
  Tasks: [],
  TotalPages: 1,
};

const getStatus = handleActions(
  {
    [actions.getTasks.REQUEST]() {
      return 'request';
    },
    [actions.getTasks.SUCCESS]() {
      return 'success';
    },
    [actions.getTasks.FAILURE]() {
      return 'failure';
    },
  },
  'none'
);

const tasksList = handleActions(
  {
    [actions.getTasks.SUCCESS](state, { payload }) {
      return {
        Tasks: payload.tasks,
        TotalPages: payload.total_task_count,
      };
    },
    [action.createTask.SUCCESS](state, { payload }) {
      return {
        ...state,
        Tasks: [...state.Tasks, payload],
      };
    },
  },
  initialState
);

const getTasks = combineReducers({
  getStatus,
  tasksList,
});

export default getTasks;
