import { takeLatest, all } from 'redux-saga/effects';

import { getTasksWorker, getTasks } from './task.action';

export function* tasksWatcher() {
  yield all([takeLatest(getTasks.TRIGGER, getTasksWorker)]);
}
