import { createRoutine } from 'redux-saga-routines';
import { put, call } from 'redux-saga/effects';

import api from '../../configs/api';

export const getTasks = createRoutine('GET_TASKS');

export function* getTasksWorker({ payload }) {
  try {
    yield put(getTasks.request());
    const res = yield call(() => api.getTasks(payload));
    if (res.data.status === 'ok') {
      yield put(getTasks.success(res.data.message));
    } else {
      yield put(getTasks.failure());
    }
  } catch (err) {
    console.log('catch', err);
    yield put(getTasks.failure());
  }
}
