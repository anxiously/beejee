import React, { Component, Fragment } from 'react';
import TaskList from './TaskList/taskList';
import NewTask from './newTask/newTask.container';
import Pagination from '../Common/Pagination';

class Tasks extends Component {
  state = {
    isFormOpen: false,
  };

  componentDidMount() {
    const { getTasks } = this.props;
    getTasks({ currentPage: 1 });
  }

  handlePageChange = props => {
    const { getTasks } = this.props;
    getTasks({ currentPage: props.selected + 1 });
  };

  toogleForm = () => {
    const { isFormOpen } = this.state;
    this.setState({
      isFormOpen: !isFormOpen,
    });
  };

  render() {
    const { isFormOpen, currentPage } = this.state;
    const { tasks, totalPage, getTasks } = this.props;
    return (
      <Fragment>
        <TaskList getTasks={getTasks} tasks={tasks} />
        <Pagination pageChange={this.handlePageChange} currentPage={currentPage} lastPage={Math.ceil(totalPage / 3)} />
        <button onClick={this.toogleForm} type="button" className="btn btn-primary btn-lg btn-block">
          Show creating task form
        </button>
        {isFormOpen === true && <NewTask />}
      </Fragment>
    );
  }
}
export default Tasks;
