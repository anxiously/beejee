import React, { PureComponent } from 'react';
import { Route, Redirect } from 'react-router-dom';

import { routes } from '../../configs/routes';

class AuthWrap extends PureComponent {
  render() {
    const { auth, exact, path, component } = this.props;
    return <Route exact={exact} path={path} component={component} />;
    //  auth ?

    // : <Redirect to={routes.login} />
  }
}

export default AuthWrap;
