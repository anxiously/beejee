import axios from 'axios';

export default {
  getTasks: data =>
    axios.get(`https://uxcandy.com/~shapoval/test-task-backend/`, {
      params: {
        developer: 'Anxiously',
        page: data.currentPage,
        sort_field: data.sortField,
        sort_direction: data.sortDirection,
      },
    }),
  createTask: data =>
    axios.post('https://uxcandy.com/~shapoval/test-task-backend/create', data, {
      params: {
        developer: 'Anxiously',
      },
    }),
};
