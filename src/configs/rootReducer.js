import { combineReducers } from 'redux';

import createTask from '../components/tasks/newTask/newTask.reducer';
import getTasks from '../components/tasks/task.reducer';

const rootReducer = combineReducers({ createTask, getTasks });

export default rootReducer;
