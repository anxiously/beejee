import { all, fork } from 'redux-saga/effects';

import { tasksWatcher } from '../components/tasks/task.saga';
import { newTasksWatcher } from '../components/tasks/newTask/newTask.saga';

export default function* rootSaga() {
  yield all([fork(tasksWatcher)]);
  yield all([fork(newTasksWatcher)]);
}
